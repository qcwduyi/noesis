/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.CustomerDirectory;
import Business.AgencyDirectory;
import java.text.ParseException;

/**
 *
 * @author AEDSpring2019
 */
public class Admin extends User {
    
    private AgencyDirectory agenDir;
    private CustomerDirectory custDir;
    
    public Admin() throws ParseException {
        super("", "", "Admin",0);
        agenDir = AgencyDirectory.getAgencySet();
        custDir = CustomerDirectory.getCustomerSet();
    }


    
    

    public AgencyDirectory getAgenDir() {
        return agenDir;
    }

    public void setAgenDir(AgencyDirectory agenDir) {
        this.agenDir = agenDir;
    }

    public CustomerDirectory getCustDir() {
        return custDir;
    }

    public void setCustDir(CustomerDirectory custDir) {
        this.custDir = custDir;
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
}
