/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ricardo
 */
public class Flight {
    private String flightNumber;
    private String departureLocation;
    private String arrivalLocation;
    private String departAirport;
    private String arriveAirport;
    private Date departureTime;
    
    private int takeoffTower;
    private Date arrivalTime;
    private int landingTower;
    private boolean isOnTime;
    private Airplane airplane;
    private Map<String, Boolean> seats;
    private int availableSeats;
    
    public Flight(String flightNumber, String departureLocation,
            String arrivalLocation, String departAirfield, String arriveAirfield, 
            String departureTime, int takeoffTower, String arrivalTime, 
            int landingTower, boolean isOnTime, Airplane airplane) throws ParseException 
            
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm");
        this.flightNumber= flightNumber;
        this.departureLocation=departureLocation;
        this.arrivalLocation=arrivalLocation;
        this.departAirport = departAirfield;
        this.arriveAirport=arriveAirfield;
        this.departureTime=dateFormat.parse(departureTime);
        this.takeoffTower = takeoffTower;
        this.arrivalTime=dateFormat.parse(arrivalTime);
        this.landingTower = landingTower;
        this.isOnTime=isOnTime;
        this.airplane = airplane;
        this.availableSeats = 150;
        initilizeSeats();
    }
    
    private void initilizeSeats()
    {
        seats = new HashMap<String, Boolean>();
        for(char alphabet = 'A'; alphabet <= 'F'; alphabet++)
        {
            for(int number = 1; number <= 25; number++)
            {
                seats.put(alphabet + String.valueOf(number),true);
            }
        }
        seats.put("A3",false);
    }
    

    public Map<String, Boolean> getSeats() {
        //System.out.println("getSeats");
        return seats;
    }

    public void setSeats(Map<String, Boolean> seats) {
        this.seats = seats;
    }
    
    
    public boolean isAvailable()
    {
        if(this.availableSeats == 0)
            return false;
        else 
            return true;
    }
    
    

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(String departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(String arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public String getDepartAirport() {
        return departAirport;
    }

    public void setDepartAirport(String departAirport) {
        this.departAirport = departAirport;
    }

    public String getArriveAirport() {
        return arriveAirport;
    }

    public void setArriveAirport(String arriveAirport) {
        this.arriveAirport = arriveAirport;
    }

    public int getTakeoffTower() {
        return takeoffTower;
    }

    public void setTakeoffTower(int takeoffTower) {
        this.takeoffTower = takeoffTower;
    }

    public int getLandingTower() {
        return landingTower;
    }

    public void setLandingTower(int landingTower) {
        this.landingTower = landingTower;
    }

    public String isIsOnTime() {
        return String.valueOf(isOnTime);
    }

    public void setIsOnTime(boolean isOnTime) {
        this.isOnTime = isOnTime;
    }
    
    
    
    
    
}
