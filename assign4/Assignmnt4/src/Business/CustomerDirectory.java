/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Agency;
import Business.Users.Customer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author AEDSpring2019
 */
public class CustomerDirectory {
    
    private static CustomerDirectory customerSets;

    public Set<Customer> customerSet;
    
    private CustomerDirectory()
    {
        Customer customer = new Customer("123","q_we@1.5",2);
        //System.out.println("customer " + customer.getUserID());
        customer.addAgency(1);
        customerSet = new HashSet<Customer>();
        customerSet.add(customer);
        //issue();
    }
    
    public void issue()
    {
        for(Customer customer : this.customerSet)
        {
            //System.out.println("customer1 " + customer.getUserID());
            System.out.println(customer.getUserID());
        }
    }

    public void addCustomer(Customer customer)
    {
        customerSet.add(customer);
    }
    
    public Customer getCumstomer(int userID)
    {
        for(Customer customer : customerSet)
        {
            if(customer.getUserID() == userID)
                return customer;
        }
        return null;
    }
    
    public static CustomerDirectory getCustomerSet() {
        customerSets = new CustomerDirectory();
        return customerSets;
    }
    
}
