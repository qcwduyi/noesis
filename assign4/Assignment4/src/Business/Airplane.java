/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author ricardo
 */
public class Airplane {
    private String airplaneName;
    private String airplaneNumber;
    private int seats;
    
    public Airplane(String airplaneName, String airplaneNumber)
    {
        this.airplaneName = airplaneName;
        this.airplaneNumber = airplaneNumber;
        this.seats = 150;
    }

    public String getAirplaneName() {
        return airplaneName;
    }

    public void setAirplaneName(String airplaneName) {
        this.airplaneName = airplaneName;
    }

    public String getAirplaneNumber() {
        return airplaneNumber;
    }

    public void setAirplaneNumber(String airplaneNumber) {
        this.airplaneNumber = airplaneNumber;
    }
    
    



    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
    
}
