/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Airliner;
import Business.AirlinerDirectory;
import Business.Flight;
import Business.Ticket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author AEDSpring2019
 */
public class Agency extends User implements Comparable<Agency>{
    
    private AirlinerDirectory directory;
    private int userID;
    private Map<Integer, Set<Ticket>> customerDirectory;
    
    public Agency(String password, String userName, int userID) {
        super(password, userName, "Agency", userID);
        directory = new AirlinerDirectory();
        this.customerDirectory = new HashMap<Integer,Set<Ticket>>();
    }
    
    

    public AirlinerDirectory getDirectory() {
        return directory;
    }
    
    public void addCustomer(int userID)
    {
        //System.out.println("agency.addCustomer + " + userID);
        Set<Ticket> tickets = new HashSet<Ticket>();
        customerDirectory.put(userID, tickets);
    }
    
    public void addTicket(Ticket ticket)
    {
        int user = ticket.getCustomerNumber();
        
      
        if(isUser(user))
            addCustomer(user);
        Set<Ticket> tickets = customerDirectory.get(user);
        tickets.add(ticket);
        customerDirectory.put(user, tickets);
        
            
        
    }
    
    private String getAirlinerID(String flightNumber)
    {
        for(Airliner airliner : directory.getAirlinerSet())
        {
            for(Flight flight : airliner.getFlightSchedule())
            {
                if(flight.getFlightNumber()==flightNumber)
                    return airliner.getAirlinerID();
            }
        }
        return null;
    }
    
    private boolean isUser(int user)
    {
        for(Map.Entry<Integer, Set<Ticket>> entry: customerDirectory.entrySet())
        {
            if(entry.getKey() == user)
                return true;
        }
        return false;
    }
    
    public Map<Integer,Set<Ticket>> getCustomerInformation()
    {
        return customerDirectory;
    }


    public void setDirectory(AirlinerDirectory directory) {
        this.directory = directory;
    }

    @Override
    public int compareTo(Agency o) {
        return o.getUserName().compareTo(this.getUserName());
    }

    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
}
