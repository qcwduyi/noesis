/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.CustomerAirliners;

import Business.Abstract.User;
import Business.Users.Admin;
import Business.Users.Agency;
import Business.Users.Customer;
import UserInterface.SuccessScreen;
import java.awt.CardLayout;
import javax.swing.JFrame;

/**
 *
 * @author ricardo
 */
public class CustomerMainScreen extends javax.swing.JFrame {

    /**
     * Creates new form CustomerMainScreen
     */
    private Customer customer;
    private JFrame mainFrame;
    private Admin admin;
    public CustomerMainScreen(JFrame mainFrame, Admin admin, Customer customer) {
        initComponents();
        this.mainFrame = mainFrame;
        this.customer = customer;
        this.admin = admin;
        grantAccessTo(customer);
    }
    
    private void grantAccessTo(User u)
    {
        SuccessScreen ss = new SuccessScreen(u);
        CardLayout layout = (CardLayout)jPanelRight.getLayout();
        jPanelRight.add(ss);
        layout.next(jPanelRight);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanelRight = new javax.swing.JPanel();
        jPanelLeft = new javax.swing.JPanel();
        ManageBtn = new javax.swing.JButton();
        SearchBtn = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelRight.setLayout(new java.awt.CardLayout());
        jSplitPane1.setRightComponent(jPanelRight);

        ManageBtn.setText("Manage");
        ManageBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageBtnActionPerformed(evt);
            }
        });

        SearchBtn.setText("Search");
        SearchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchBtnActionPerformed(evt);
            }
        });

        btnExit.setText("Exit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelLeftLayout = new javax.swing.GroupLayout(jPanelLeft);
        jPanelLeft.setLayout(jPanelLeftLayout);
        jPanelLeftLayout.setHorizontalGroup(
            jPanelLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(ManageBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        jPanelLeftLayout.setVerticalGroup(
            jPanelLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLeftLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(ManageBtn)
                .addGap(31, 31, 31)
                .addComponent(SearchBtn)
                .addGap(18, 18, 18)
                .addComponent(btnExit)
                .addContainerGap(439, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanelLeft);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 653, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(102, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 637, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 614, Short.MAX_VALUE)
                    .addGap(12, 12, 12)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ManageBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageBtnActionPerformed
        // TODO add your handling code here:

        CardLayout layout = (CardLayout)jPanelRight.getLayout();
        jPanelRight.add(new AddAgency(admin, customer, jPanelRight));
        layout.next(jPanelRight);
    }//GEN-LAST:event_ManageBtnActionPerformed

    private void SearchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchBtnActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout)jPanelRight.getLayout();
        jPanelRight.add(new TicketInformation(customer, jPanelRight));
        layout.next(jPanelRight);
    }//GEN-LAST:event_SearchBtnActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        // TODO add your handling code here:

        this.dispose();
        admin.getCustDir().addCustomer(customer);
        for(int agency : customer.getAgencyDirectory())
        {
            admin.getAgenDir().getAgency(agency).addCustomer(customer.getUserID());
        }

        mainFrame.setVisible(true);
    }//GEN-LAST:event_btnExitActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ManageBtn;
    private javax.swing.JButton SearchBtn;
    private javax.swing.JButton btnExit;
    private javax.swing.JPanel jPanelLeft;
    private javax.swing.JPanel jPanelRight;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
