/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import static java.util.Map.Entry.comparingByValue;
import java.util.stream.Collectors;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    
    public void userWithMostLikes(){
        Map<Integer,Integer> userLikecount = new HashMap<Integer, Integer>();
        
        Map<Integer, User>users = DataStore.getInstance().getUsers();
        for(User user : users.values()){
            for(Comment c: user.getComments()){
            int likes = 0;
            if(userLikecount.containsKey(user.getId()))
                    likes = userLikecount.get(user.getId());
            likes+=c.getLikes();
            userLikecount.put(user.getId(),likes);
            }
        }
                int max = 0;
                int maxId =0;
                for(int id: userLikecount.keySet()){
                    if(userLikecount.get(id)>max){
                        max=userLikecount.get(id);
                            maxId=id;
                            }
                }
                //System.out.println("\nUser with most likes: "+max+"\n"+users.get(maxId));
    }
    
    public void getFiveMostLikedComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        Collections.sort(commentList, new Comparator<Comment>(){
       
        @Override
        public int compare(Comment o1, Comment o2){
        return o2.getLikes()-o1.getLikes();
        }
        });
        
        System.out.println("\n5 most liked comments:");
        for (int i=0; i< commentList.size()&&i<5;i++){
            System.out.println(commentList.get(i));
        }
        
    }
    public void getPostWithMostLikedComment(){
        Map<Integer,Integer> postLikecount = new HashMap<Integer, Integer>();
        
        Map<Integer, Post>post = DataStore.getInstance().getPosts();
       
        for(Post posts : post.values()){
             
            for(Comment c: posts.getComments()){
            int likes = 0;
            if(postLikecount.containsKey(posts.getPostId()))
                    likes = postLikecount.get(posts.getPostId());
            likes+=c.getLikes();
            postLikecount.put(posts.getPostId(),likes);
          }
        }
                int max = 0;
                int maxId =0;
                for(int id: postLikecount.keySet()){
                    if(postLikecount.get(id)>max){
                        max=postLikecount.get(id);
                            maxId=id;
                            }
                }
                System.out.println("\nPost with most likes: "+max+"\n"+post.get(maxId));
        
    }
     public void getavgLikesPerComment() {
        int likes = 0, noOfComment = 0;
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        for (Comment c : comments.values()) {
            //System.out.println(c);

            likes += c.getLikes();
            noOfComment++;

        }
       // System.out.println("\n Find Average number of likes per comment.");
     //   System.out.println("\nAverage number of likes per comment is the total number of likes divide by total number comments");
       // System.out.println();
       // System.out.println("Total number of likes = " + likes);
//        System.out.println("Total number of comments =" + noOfComment);

        int average = 0;
        average = likes / noOfComment;
        System.out.println("Average number of likes per comment = " + average);

    }
     
   
      public void postWithMostComments() {

        Map<Integer, Integer> noOfCommentsCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        for (Post post : posts.values()) {
            int CommentsCount = 0;
            for (Comment comment : post.getComments()) {
                if (noOfCommentsCount.containsKey(post.getPostId())) {
                    CommentsCount++;
                } else {
                    CommentsCount++;
                }
                noOfCommentsCount.put(post.getPostId(), CommentsCount);
            }
        }

        int max = 0;
        int maxId = 0;
        for (int id : noOfCommentsCount.keySet()) {
            if (noOfCommentsCount.get(id) > max) {
                max = noOfCommentsCount.get(id);
                maxId = id;
            }
        }
       // System.out.println("\nPost Id with most comments: " + maxId);
        System.out.println("Post with most comments: " + max);
    }
    
    public void getFiveMostInactiveUsers()
    {
        Map<Integer, Integer> userOverall = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        for(Map.Entry<Integer, User> entry : users.entrySet())
        {
            userOverall.put(entry.getKey(), 0);
        }
        for(Map.Entry<Integer, Comment> entry : comments.entrySet())
        {
            for(int userID : userOverall.keySet())
            {
                if(userID == entry.getValue().getUserId())
                {
                    //if(userID == 7)
                    //System.out.println("userID: " + userID + "number: " + userOverall.get(userID));
                    int number = userOverall.get(userID) + entry.getValue().getLikes();
                    //if(userID == 7)
                    //System.out.println("userID: " + userID + "number: " + number);
                    userOverall.put(userID, ++number);
                    //if(userID == 7)
                    //System.out.println("userID: " + userID + "number: " + number);
                }
            }
        }
        
        for(Map.Entry<Integer, Post> entry : posts.entrySet())
        {
            for(int userID : userOverall.keySet())
            {
                if(userID == entry.getValue().getUserId())
                {
                    int number = userOverall.get(userID);
                    //if(userID == 7)
                    //System.out.println("userID: " + userID + "number: " + number);
                    userOverall.put(userID, ++number);
                    //if(userID == 7)
                    //System.out.println("userID: " + userID + "number: " + number);
                }
            }
        }
        
        List<Map.Entry<Integer, Integer>> sortedUser = new 
        LinkedList<Map.Entry<Integer, Integer>>(userOverall.entrySet());
        
        Collections.sort(sortedUser, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return o1.getValue() - o2.getValue(); 
            } 
        });
        
        System.out.println("Top 5 inactive users overall: ");
        for(int i = 0; i < sortedUser.size() && i < 5; ++i )
        {
            System.out.println("userID: " + sortedUser.get(i).getKey() + "; " 
                    + "Overall sum: " + sortedUser.get(i).getValue());
        }

       
    }
    
    public void getFiveMostProactiveUsers()
    {
        Map<Integer, Integer> userOverall = new HashMap<>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        for(Map.Entry<Integer, User> entry : users.entrySet())
            userOverall.put(entry.getKey(), 0);
        for(Map.Entry<Integer, Comment> entry : comments.entrySet())
        {
            for(int userID : userOverall.keySet())
            {
                if(userID == entry.getValue().getUserId())
                {
                    int number = userOverall.get(userID) + entry.getValue().getLikes();
                    userOverall.put(userID, ++number);
                }
            }
        }
        
        for(Map.Entry<Integer, Post> entry : posts.entrySet())
        {
            for(int userID : userOverall.keySet())
            {
                if(userID == entry.getValue().getUserId())
                {
                    int number = userOverall.get(userID);
                    userOverall.put(userID, ++number);
                }
            }
        }
        
        List<Map.Entry<Integer, Integer>> sortedUser = new 
        LinkedList<Map.Entry<Integer, Integer>>(userOverall.entrySet());
        
        Collections.sort(sortedUser, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return o2.getValue() - o1.getValue(); 
            } 
        });
        
        System.out.println("Top 5 proactive users overall: ");
        for(int i = 0; i < sortedUser.size() && i < 5; ++i )
        {
            System.out.println("userID: " + sortedUser.get(i).getKey() + "; " 
                    + "Overall sum: " + sortedUser.get(i).getValue());
        }

       
    }
     public void Top5InactiveUsersOnTotalPostsNumber(){       
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer,Integer> userPostCount = new HashMap<>();
               
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(posts.values());
     
        
        for(User user:users.values()){
            int count = 0;
            for(Post post:postList){
                if(post.getUserId()==user.getId()){             
                    count++;
                    userPostCount.put(user.getId(), count);              
                }
            }         
        }
        List<Map.Entry<Integer,Integer>> userPostCountList = new ArrayList<>(userPostCount.entrySet());
        Collections.sort(userPostCountList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Map.Entry<Integer,Integer> c1, Map.Entry<Integer,Integer> c2){
                return c1.getValue() - c2.getValue();
            }
        });
        
        System.out.println("Top 5 inactive users based on total posts number: ");
        for(int i=0;i<userPostCountList.size()&&i<5;i++){
                System.out.println("User: No."+userPostCountList.get(i).getKey()+" Post Number: "+userPostCountList.get(i).getValue()); 
        }     
    }
       
        public void getFiveMostInactiveUsersOnCommentNumberMethod1(){       
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        Map<Integer,Integer> userCommentCount = new HashMap<>();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        for(User user:users.values()){
            int count = 0;
            for(Comment comment:commentList){
                if(comment.getUserId()==user.getId()){             
                    count++;
                    userCommentCount.put(user.getId(), count);              
                }
            }         
        }
        List<Map.Entry<Integer,Integer>> userCommentCountList = new ArrayList<>(userCommentCount.entrySet());
        Collections.sort(userCommentCountList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Map.Entry<Integer,Integer> c1, Map.Entry<Integer,Integer> c2){
                return c1.getValue() - c2.getValue();
            }
        });
        
        System.out.println("Top 5 inactive users based on total comments number: ");
        for(int i=0;i<userCommentCountList.size()&&i<5;i++){
                System.out.println("User: No."+userCommentCountList.get(i).getKey()+" Comments Number: "+userCommentCountList.get(i).getValue()); 
        }     
    } 
}
