/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Product;
import com.assignment5.entities.Order;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author vanita
 */
public class AnalysisHelper {
 
    private Map<Integer, Float> prod_avg = new HashMap<>();
    public void top3Products() {

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        Map<Integer, Integer> prod_target = new HashMap<>();
        Map<Integer, Integer> prod_profit = new HashMap<>();

        for (Product p : products.values()) {
            prod_target.put(p.getProductId(), p.getTargetPrice());
        }
        for (Order o : orders.values()) {
            prod_profit.put(o.getItem().getProdId(), 0);
        }
        int p = 0;

        for (Order o : orders.values()) {
            p = prod_profit.get(o.getItem().getProdId());
            if (o.getItem().getSalesPrice() > prod_target.get(o.getItem().getProdId())) {

                p += o.getItem().getQuantity();

            }
            prod_profit.put(o.getItem().getProdId(), p);

        }

        List<Integer> mapKeys = new ArrayList<>(prod_profit.keySet());
        List<Integer> mapValues = new ArrayList<>(prod_profit.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = prod_profit.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> prodid = new ArrayList<Integer>(sortedMap.keySet());
        System.out.println("\n Q1-Top 3 Products are:\r");
        Collections.reverse(prodid);
         for (int x = 0; x < 3; x++) {

            System.out.println("Product No: " + prodid.get(x));
         }
        
        System.out.println(prod_profit);
    }

public void top3Customers() {
        LinkedHashMap<Integer, Integer> topcustomers = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();

        for (Order o : orders.values()) {
            if (topcustomers.containsKey(o.getCustomerId())) {
                int sum = topcustomers.get(o.getCustomerId());
                topcustomers.put(o.getCustomerId(), (sum) + (o.getItem().getQuantity() * o.getItem().getSalesPrice()));

            } else {
                topcustomers.put(o.getCustomerId(), o.getItem().getQuantity() * o.getItem().getSalesPrice());
            }

        }

        List<Integer> mapKeys = new ArrayList<>(topcustomers.keySet());
        List<Integer> mapValues = new ArrayList<>(topcustomers.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = topcustomers.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> customer = new ArrayList<Integer>(sortedMap.keySet());
        Collections.reverse(customer);

        System.out.println("\n Q2-Top 3 Customers are:\r");
        for (int x = 0; x < 3; x++) {

            System.out.println("Customer No: " + customer.get(x));
        }

    }

     public void top3salesperson() {
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        Map<Integer, Integer> prod_target = new HashMap<>();
        Map<Integer, Integer> saleid_profit = new HashMap<>();//last map
        for (Product p : products.values()) {
            prod_target.put(p.getProductId(), p.getTargetPrice());
        }
        for (Order o : orders.values()) {
            saleid_profit.put(o.getSalesId(), 0);
        }

        int p = 0;
        for (Order o : orders.values()) {
            p = saleid_profit.get(o.getSalesId());

            p += o.getItem().getQuantity() * (o.getItem().getSalesPrice() - prod_target.get(o.getItem().getProdId()));
            saleid_profit.put(o.getSalesId(), p);

        }

        List<Integer> mapKeys = new ArrayList<>(saleid_profit.keySet());
        List<Integer> mapValues = new ArrayList<>(saleid_profit.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            int val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                int comp1 = saleid_profit.get(key);
                int comp2 = val;

                if (comp1 == comp2) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }

        ArrayList<Integer> salesPerson = new ArrayList<Integer>(sortedMap.keySet());
        Collections.reverse(salesPerson);
        System.out.println("\n Q3-Top 3 Sales Persons are:\r");
        for (int x = 0; x < 3; x++) {
            System.out.println("Sales Person: " + salesPerson.get(x));
        }
       

        int sum = 0;
        for (int i : sortedMap.values()) {
            if (i > 0) {
                sum = sum + i;
            }
        }
        System.out.println("Sum of Profits made by sales persons:" + sum);
        System.out.println(saleid_profit);
    }
     
     
 public void totalRevenue() {

        LinkedHashMap<Integer, Integer> totalRevenue = new LinkedHashMap<Integer, Integer>();

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();

        for (Order o : orders.values()) {
            int prod = o.getItem().getProdId();

            for (Product p : products.values()) {
                if (p.getProductId() == prod) {
                    {

                        if (totalRevenue.containsKey(o.getOrderId())) {
                            int sum = totalRevenue.get(o.getOrderId());
                            totalRevenue.put(o.getOrderId(), (sum) + (o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice())));

                        } else {
                            totalRevenue.put(o.getOrderId(), o.getItem().getQuantity() * (o.getItem().getSalesPrice() - p.getMinPrice()));

                        }
                    }
                }
            }
        }
        int totalRev = 0;
        for (int i = 0; i < totalRevenue.size(); i++) {
            totalRev = totalRev + totalRevenue.get(i);
        }
        System.out.println("\n Q4-Total Revenue generated with respect to Sales Data: " + totalRev +"\n\r");    
    }
    
        public void modifyingTargetpriceToOptimumlevels() {
        
        Map<Integer, Map<Integer,Integer>> targetProducts = new HashMap<Integer, Map<Integer,Integer>>();
        Map<Integer, Product> products = DataStore.getInstance().getProduct();
        for(Product product : products.values())
        {
            Map<Integer, Integer> value = new HashMap<Integer, Integer>();
            value.put(1, 0);
            value.put(2, 0);
            value.put(3, product.getTargetPrice());
            targetProducts.put(product.getProductId(), value);
        }
                
        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        for(Order order : orders.values())
        {
            for(Map.Entry<Integer, Map<Integer,Integer>> entry : targetProducts.entrySet())
            {
                if(entry.getKey() == order.getItem().getProdId())
                {
                    
                    
                    Map<Integer, Integer> value = entry.getValue();
                    int totalPrice = order.getItem().getSalesPrice() 
                            * order.getItem().getQuantity() + value.get(2);
                    int quantity = order.getItem().getQuantity() + value.get(1);
                    
                    value.put(1, quantity);
                    value.put(2, totalPrice);
                    entry.setValue(value);
                    
                }
            }
        }
        
        List<Map.Entry<Integer, Map<Integer,Integer>>> sortedUser = new 
        LinkedList<Map.Entry<Integer, Map<Integer,Integer>>>(targetProducts.entrySet());
        
        Collections.sort(sortedUser, new Comparator<Map.Entry<Integer, Map<Integer,Integer>> >() { 
            public int compare(Map.Entry<Integer, Map<Integer,Integer>> o1,  
                               Map.Entry<Integer, Map<Integer,Integer>> o2) 
            { 
                int averageSalePriceO1 = o1.getValue().get(2) / o1.getValue().get(1);
                int differenceO1 = averageSalePriceO1 - o1.getValue().get(3);
                int averageSalePriceO2 = o2.getValue().get(2) / o2.getValue().get(1);
                int differenceO2 = averageSalePriceO2 - o2.getValue().get(3);
                return differenceO2 - differenceO1; 
            } 
        });
        
        System.out.println("\n\nOriginal data: ");
        
        for(int i = 0; i < sortedUser.size(); ++i )
        {
            float averageSalePrice = sortedUser.get(i).getValue().get(2) / 
                    sortedUser.get(i).getValue().get(1);
            
            System.out.println("productID: " + sortedUser.get(i).getKey() + 
                    " ,Average sale price: " + averageSalePrice + 
                    " ,Target price: " + sortedUser.get(i).getValue().get(3) + 
                    " ,The difference: " + (float)(averageSalePrice - sortedUser.get(i).getValue().get(3)));
        }

        System.out.println("-----------------");
        System.out.println("Modified data: ");
        Random rand = new Random();
        int modifiedTargetPrice;
        
        for(int i = 0; i < sortedUser.size(); ++i )
        {
            int randInt = rand.nextInt(5);
            float averageSalePrice = sortedUser.get(i).getValue().get(2) / 
                    sortedUser.get(i).getValue().get(1);
            int difference = (int)(averageSalePrice - sortedUser.get(i).getValue().get(3));
            
            if(difference > 0)
            {
                modifiedTargetPrice = (int)(averageSalePrice * (100 - randInt) / 100);
            }else if(difference < 0){
                modifiedTargetPrice = (int)(averageSalePrice * (100 + randInt) / 100);
            }else {
                modifiedTargetPrice = sortedUser.get(i).getValue().get(3);
            }
            System.out.println("productID: " + sortedUser.get(i).getKey() + 
                    " ,Average sale price: " + averageSalePrice + 
                    " ,Modified targrt price: " + modifiedTargetPrice + 
                    " ,The difference: " + (float)(averageSalePrice - sortedUser.get(i).getValue().get(3)) + 
                    " ,The error: " + (float)((modifiedTargetPrice - averageSalePrice)/averageSalePrice));
        }
    }
  

}
