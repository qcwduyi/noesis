/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Item {
    int itemId;
    int salesPrice;
    int quantity;
    Product product;
    int prodId;
  
        
        public Item(int itemId, int salesPrice, int quantity,int prodId) {
        this.itemId = itemId;
        this.salesPrice = salesPrice;
        this.quantity = quantity;
        this.prodId = prodId;
        
    }

    public int getItemId() {
        return itemId;
    }
    
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }
    
     public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
  
}