/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author AEDSpring2019
 */
public class AirlinerDirectory {
    
    private Set<Airliner> airlinerSet;// = new ArrayList<Product>();
    
    public AirlinerDirectory(){
        airlinerSet = new HashSet<>();
    }
    
    public void addAirline(Airliner airliner)
    {
        airlinerSet.add(airliner);
    }
    
    public void deleteAirline(Airliner airliner)
    {
        airlinerSet.remove(airliner);
    }
    
    public Airliner gerAirliner(String airlinerID)
    {
        for(Airliner airliner: airlinerSet)
        {
            if(airliner.getAirlinerID().equals(airlinerID))
                return airliner;
        }
        return null;
    }

    public Set<Airliner> getAirlinerSet() {
        return airlinerSet;
    }


    
    
    
}
