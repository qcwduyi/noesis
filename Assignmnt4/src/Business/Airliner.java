/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author AEDSpring2019
 */
public class Airliner implements Comparable<Airliner>{
    private String name;
    private String airlinerID;
    private ArrayList<Airplane> fleet;
    private ArrayList<Flight> flightSchedule;

    public Airliner(String name, String airlinerID) {
        this.name = name;
        this.airlinerID = airlinerID;
        fleet = new ArrayList<Airplane>();
        flightSchedule = new ArrayList<Flight>();
    }

    public ArrayList<Airplane> getFleet() {
        return fleet;
    }

    public void setFleet(ArrayList<Airplane> fleet) {
        this.fleet = fleet;
    }

    public ArrayList<Flight> getFlightSchedule() {
        return flightSchedule;
    }

    public void setFlightSchedule(ArrayList<Flight> flightSchedule) {
        this.flightSchedule = flightSchedule;
    }
    
    public Flight getFlight(String flightNumber)
    {
        for(Flight flight: flightSchedule)
        {
            if(flight.getFlightNumber().equals(flightNumber))
                return flight;
        }
        return null;
    }
    
    
    
    public void addAirplane(Airplane airplane)
    {
        fleet.add(airplane);
    }
    
    public void addFlight(Flight flight)
    {
        flightSchedule.add(flight);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAirlinerID() {
        return airlinerID;
    }

    public void setAirlinerID(String airlinerID) {
        this.airlinerID = airlinerID;
    }

    

    @Override
    public int compareTo(Airliner o) {
        return this.name.compareTo(o.getName());
    }

    
}
