/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.AirlinerDirectory;
import Business.Ticket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author AEDSpring2019
 */
public class Customer extends User implements Comparable<Agency>{
    
    private AirlinerDirectory directory;
    
    private int userID;
    private Date date;
    private Set<Integer> agencyDirectory;
    private Set<Ticket> ticketDirectory;
    
    public Customer(String password, String userName, int userID) {
        super(password, userName, "Customer", userID);
        directory = new AirlinerDirectory();
        date = Calendar.getInstance().getTime();
        this.userID = userID;
        agencyDirectory = new HashSet<Integer>(); 
        ticketDirectory = new HashSet<Ticket>();

    }

    public Set<Integer> getAgencyDirectory() {
        return agencyDirectory;
    }
    
    public void addAgency(int agencyID) {
        agencyDirectory.add(agencyID);
    }
    
    public void addTicket(Ticket ticket)
    {
        System.out.println("addTicket");
        ticketDirectory.add(ticket);
    }

    public Set<Ticket> getTicketDirectory() {
        return ticketDirectory;
    }

    
    

    public int getUserID() {
        return userID;
    }

    
    
    
    

    public AirlinerDirectory getDirectory() {
        return directory;
    }

    public Date getDate() {
        return date;
    }

    
    

    public void setDirectory(AirlinerDirectory directory) {
        this.directory = directory;
    }

    @Override
    public int compareTo(Agency o) {
        return o.getUserName().compareTo(this.getUserName());
    }

    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
}
