/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Agency;
import Business.Users.Customer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author AEDSpring2019
 */
public class AgencyDirectory {
    
    private static AgencyDirectory agencySets;
    
    public Set<Agency> agencySet;
    
    private AgencyDirectory() throws ParseException
    {
        Airplane airplane1 = new Airplane("Boinplane", "B100");
        
        Airplane airplane2 = new Airplane("Boinplane", "B200");
        
        Airplane airplane3 = new Airplane("Boinplane", "B300");
        
        Flight flight1 = new Flight("A001", "Beijing", "Boston", 
                "Beijing International Airport", "Logan International Airport", 
                "2019/09/15 23:15", 1, "2019/09/15 06:15", 1, true, airplane1);
    
        Flight flight2 = new Flight("A002", "New Jersey", "New York", 
                "Teterboro Airport", "Laguardia Airport", 
                "2019/09/15 23:15", 1, "2019/09/16 00:15", 1, true, airplane2);
        
        Flight flight3 = new Flight("A100","San Fransico", "New York",
                "San Fransico Airport","New York Airport", "2019/09/15 10:15",2, 
                "2019/09/15 23:15",1,true,airplane2);
        
        Flight flight4 = new Flight("A101", "Utah", "New York",
                "San Fransico Airport","New York Airport", 
                "2019/09/15 13:15 ", 1,"2019/09/15 18:25:00",1,true,airplane1);
        
       Flight flight5 = new Flight("A105", "San Jose", "New York",
                "San Jose Airport", "New York Airport", 
                "2019/09/16 15:15 ", 1,"2019/09/16 20:45:00",1,true,airplane3);
       Flight flight6 = new Flight("A106", "San Diego", "New York",
                "San Diego Airport", "New York Airport", 
                "2019/09/16 12:30 ", 1,"2019/09/16 18:30:00",1,true,airplane2);
       Flight flight7 = new Flight("A107",  "Washington DC", "New York",
                 "Washington DC Airport", "New York Airport", 
                "2019/09/15 08:35 ", 1,"2019/09/15 11:45:00",1,true,airplane1);
       Flight flight8 = new Flight("A108","Illinois", "New York",
                "Illinois Airport", "New York Airport" ,
                "2019/09/15 23:15 ", 1,"2019/09/16 04:30:00",1,true,airplane3);
       Flight flight9 = new Flight("A109",  "Canada", "New York",
                 "Canada Airport", "New York Airport", 
                "2019/09/17 22:15 ", 1,"2019/09/18 10:05:00",1,true,airplane1);
       Flight flight10 = new Flight("A110", "Boston", "New York",
                "Boston Airport", "New York Airport", 
                "2019/09/16 08:55 ", 1,"2019/09/15 12:05:00",1,true,airplane2);
       Flight flight11 = new Flight("A111", "New Jersey", "New York",
                "New Jersey Airport", "New York Airport", 
                "2019/09/15 18:20 ", 1,"2019/09/15 23:40:00",1,true,airplane2);
       Flight flight12 = new Flight("A112", "San Fransico", "New York",
                "San Fransico Airport", "New York Airport", 
                "2019/09/17 16:35 ", 1,"2019/09/15 19:55:00",1,true,airplane1);
       Flight flight13 = new Flight("A113", "Utah", "New York",
                "Utah Airport", "New York Airport", 
                "2019/09/16 01:15 ", 1,"2019/09/15 08:05:00",1,true,airplane2);
       Flight flight14 = new Flight("A114", "Utah", "New York",
                "San Fransico Airport","New York Airport", 
                "2019/09/15 13:15 ", 1,"2019/09/15 18:25:00",1,true,airplane3);
       Flight flight15 = new Flight("A115", "San Jose", "New York",
                "San Jose Airport", "New York Airport", 
                "2019/09/16 19:40 ", 1,"2019/09/15 23:45:00",1,true,airplane1);
       Flight flight16 = new Flight("A116",  "Washington DC", "New York",
                 "Washington DC Airport", "New York Airport", 
                "2019/09/17 09:45 ", 1,"2019/09/15 13:10:00",1,true,airplane2);
       Flight flight17 = new Flight("A117", "Illinois", "New York",
                "Illinois Airport", "New York Airport",
                "2019/09/16 23:15 ", 1,"2019/09/17 08:50:00",1,true,airplane2);
       Flight flight18 = new Flight("A118", "Canada", "New York",
                "Canada Airport", "New York Airport", 
                "2019/09/16 15:50 ", 1,"2019/09/15 20:40:00",1,true,airplane1);
       Flight flight19 = new Flight("A119",  "Boston", "New York",
                 "Boston Airport", "New York Airport", 
                "2019/09/15 03:35 ", 1,"2019/09/15 09:00:00",1,true,airplane3);
       Flight flight20 = new Flight("A120","New Jersey", "New York",
                "New Jersey Airport", "New York Airport",
                "2019/09/17 06:30 ", 1,"2019/09/15 14:40:00",1,true,airplane1);
       Flight flight21 = new Flight("A121", "San Fransico", "New York",
                 "San Fransico Airport", "New York Airport",
                "2019/09/17 18:30 ", 1,"2019/09/15 23:40:00",1,true,airplane2);
       Flight flight22 = new Flight("A122","New Jersey", "New York",
                "New Jersey Airport", "New York Airport",
                "2019/09/17 06:30 ", 1,"2019/09/15 14:40:00",1,true,airplane1);
       Flight flight23 = new Flight("A123", "San Jose", "New York",
                 "San Jose Airport", "New York Airport",
                "2019/09/15 04:30 ", 1,"2019/09/15 14:00:00",1,true,airplane3);
       Flight flight24 = new Flight("A124", "San Diego", "New York",
                 "San Diego Airport", "New York Airport",
                "2019/09/16 16:30 ", 1,"2019/09/15 22:20:00",1,true,airplane1);
       Flight flight25 = new Flight("A125", "Washington DC", "New York",
                 "Washington DC Airport", "New York Airport",
                "2019/09/17 12:35 ", 1,"2019/09/15 14:45:00",1,true,airplane1);
       Flight flight26 = new Flight("A126", "Illinois", "New York",
                 "Illinois Airport", "New York Airport",
                "2019/09/17 11:50 ", 1,"2019/09/17 15:55:00",1,true,airplane2);
       Flight flight27 = new Flight("A127", "Canada", "New York",
                "New Jersey Airport", "New York Airport",
                "2019/09/17 06:30 ", 1,"2019/09/15 14:40:00",1,true,airplane1);
      
       
        


        
        
        ArrayList<Flight> flightList1 = new ArrayList<Flight>();
        ArrayList<Flight> flightList2 = new ArrayList<Flight>();
        flightList1.add(flight1);
        flightList2.add(flight2);
        flightList2.add(flight3);
        flightList1.add(flight4);
        flightList2.add(flight5);
        flightList2.add(flight6);
        flightList1.add(flight7);
        flightList2.add(flight8);
        flightList1.add(flight9);
        flightList2.add(flight10);
        flightList2.add(flight11);
        flightList1.add(flight12);
        flightList2.add(flight13);
        flightList2.add(flight14);
        flightList1.add(flight15);
        flightList2.add(flight16);
        flightList2.add(flight17);
        flightList1.add(flight18);
        flightList2.add(flight19);
        flightList1.add(flight20);
        flightList2.add(flight21);
        flightList1.add(flight22);
        flightList2.add(flight23);
        flightList1.add(flight24);
        flightList1.add(flight25);
        flightList2.add(flight26);
        flightList1.add(flight27);
       
        
        
        
        
        
        
        
        
        
        
        
        ArrayList<Airplane> fleet1 = new ArrayList<Airplane>();
        ArrayList<Airplane> fleet2 = new ArrayList<Airplane>();
        
        fleet1.add(airplane1);
        fleet2.add(airplane2);
        fleet2.add(airplane3);
        
        
        Airliner airliner1 = new Airliner("Hainan Airlines", "H001");
        Airliner airliner2 = new Airliner("American Airlines", "A001");
        
        airliner1.setFleet(fleet1);
        airliner1.setFlightSchedule(flightList1);
        airliner2.setFleet(fleet2);
        airliner2.setFlightSchedule(flightList2);
        
        agencySet = new HashSet<Agency>();
        Agency agency1 = new Agency("123","q_c@1.2",1);
        agency1.getDirectory().addAirline(airliner1);
        agency1.getDirectory().addAirline(airliner2);
        agency1.addCustomer(2);
        
        agencySet.add(agency1);

        /*
        Airplane airplane1 = new Airplane("Boinplane", "B100");
        
        Airplane airplane2 = new Airplane("Boinplane", "B200");
        
        
        Flight flight1 = new Flight("A001", "Beijing", "Boston", 
                "Beijing International Airport", "Logan International Airport", 
                "2019/09/15 23:15", 1, "2019/09/15 06:15", 1, true, airplane1);
    
        Flight flight2 = new Flight("A002", "New Jersey", "New York", 
                "Teterboro Airport", "Laguardia Airport", 
                "2019/09/15 23:15", 1, "2019/09/16 00:15", 1, true, airplane2);
        
        Flight flight3 = new Flight("A100","San Fransico", "New York",
                "San Fransico Airport","New York Airport", "2019/09/15 10:15",2, 
                "2019/09/15 23:15",1,true,airplane2);
        
        
        ArrayList<Flight> flightList1 = new ArrayList<Flight>();
        
        flightList1.add(flight1);
        flightList1.add(flight2);
        
        ArrayList<Airplane> fleet1 = new ArrayList<Airplane>();
        
        fleet1.add(airplane1);
        
        Airliner airliner1 = new Airliner("Haina Airlines", "H001");
        
        airliner1.setFleet(fleet1);
        airliner1.setFlightSchedule(flightList1);
        
        agencySet = new HashSet<Agency>();
        Agency agency1 = new Agency("123","q_c@1.2",1);
        agency1.getDirectory().addAirline(airliner1);
        agency1.addCustomer(2);
        
        agencySet.add(agency1);
        
        */
        
    }
    


    public void addAgency(Agency agency)
    {
        agencySet.add(agency);
    }
    
    public Agency getAgency(int userID)
    {
        for(Agency agency: agencySet)
        {
            if(agency.getUserID() == userID)
                return agency;
        }
        return null;
    }
    
    public static AgencyDirectory getAgencySet() throws ParseException {
        agencySets = new AgencyDirectory();
        return agencySets;
    }
    
    
}
