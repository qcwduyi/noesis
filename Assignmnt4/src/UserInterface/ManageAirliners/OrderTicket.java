/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.ManageAirliners;

import Business.Flight;
import Business.Ticket;
import Business.Users.Agency;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author ricardo
 */
public class OrderTicket extends javax.swing.JFrame {

    /**
     * Creates new form OrderTicket
     */
    private int orderer;
    private Flight flight;
    private Agency agency;
    private static int number = 0;
    private JFrame jFrame;
    public OrderTicket(JFrame jFrame,int orderer, Flight flight, Agency agency) {
        initComponents();
        this.orderer = orderer;
        this.flight = flight;
        this.agency = agency;
        this.jFrame = jFrame;
        populate();
    }
    
    public void populate()
    {
        DefaultTableModel dtm = (DefaultTableModel) Seats.getModel();
        dtm.setRowCount(0);
        Seats.setCellSelectionEnabled(true);
        
        for(int row = 0; row < 25; ++ row)
        {
            Object[] object = new Object[6];
            for(int column = 0; column < 6 ; column ++)
            {
                
                object[column] = (char)('A'+column) + String.valueOf(row + 1);
                if(!flight.getSeats().get(object[column]))
                {
                    //System.out.println("Row = " + row + "Column = " + column);
                    //DefaultTableCellRenderer dtcr = (DefaultTableCellRenderer) Seats.getCellRenderer(row, column);//r
                    //dtcr.setBackground(Color.red);
                    //setTicket(row, column);
                    object[column] = (char)('a'+column) + String.valueOf(row + 1);
                    

                }
                    
                
            }  

            dtm.addRow(object);
  
        }

    }
   /*
    private void setTicket(int row, int column)
    {
        System.out.println("Row = " + row + "Column = " + column);
        
        //TableCellRenderer renderer = Seats.getCellRenderer(row, column);
        //TableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        //Component c = Seats.prepareRenderer(renderer, 1, 1);
        //cellRenderer.setBackground(Color.RED);
        //c.setBackground(Color.red);
        
        //Component component = Seats.prepareRenderer(renderer, row, column);
        
    }*/

    
   
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPasswordField1 = new javax.swing.JPasswordField();
        btnOrder = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Seats = new javax.swing.JTable();

        jPasswordField1.setText("jPasswordField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnOrder.setText("Order");
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });
        getContentPane().add(btnOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(207, 22, -1, -1));

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        getContentPane().add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 22, -1, -1));

        Seats.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "A", "B", "C", "D", "E", "F"
            }
        ));
        jScrollPane1.setViewportView(Seats);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 69, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrderActionPerformed
        // TODO add your handling code here:

        int column = Seats.getSelectedColumn();
        int row = Seats.getSelectedRow();
        String string = (char)('A'+column) + String.valueOf(row + 1);
        if(column >= 0 && row >= 0)
        {
            if(!flight.getSeats().get(string))
            {
                JOptionPane.showMessageDialog(null, "The seat has been ordered!");
                return;
            }
            DefaultTableModel tableModel = (DefaultTableModel) Seats.getModel();
            
            //System.out.println("row = " + row);
            //System.out.println("column = " + column);

            String cellValue = (String) tableModel.getValueAt(row, column);
            
            //System.out.println("cellValue = " + cellValue);
            
            Ticket ticket = new Ticket();
            ticket.setAgencyName(agency.getUserName());
            ticket.setAgencyNumber(agency.getUserID());
            ticket.setCustomerNumber(orderer);
            ticket.setFlightNumber(flight.getFlightNumber());
            ticket.setSeat(cellValue);
            ticket.setTicketNumber(++ number);
            
            flight.getSeats().put(cellValue, false);
            agency.addTicket(ticket);
            //setBackground(row, column);
            populate();
            JOptionPane.showMessageDialog(null, "Order is added successfully!");
        }else
            JOptionPane.showMessageDialog(null, "Please select one cell!");


        
    }//GEN-LAST:event_btnOrderActionPerformed

    private boolean isRedCell(int row, int column)
    {
        TableCellRenderer renderer = Seats.getCellRenderer(row, column);
        Component c = Seats.prepareRenderer(renderer, row, column);
        return Color.RED == c.getBackground();
    }

    
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        this.dispose();
        jFrame.setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Seats;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnOrder;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
